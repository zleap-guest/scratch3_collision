# scratch3_collision

Following on from the movement tutorial.  Lets look at what happens if our sprite was to collide with another. 

Below is a backdrop with a rocket sprite and some rocks. 

![collision2a](https://salsa.debian.org/zleap-guest/scratch3_collision/-/raw/master/collision2a.png)

We need to make the rocket sprite move when the arrow keys are pressed.

![rocket-move](https://salsa.debian.org/zleap-guest/scratch3_collision/-/raw/master/4-keys.png)

Once this is working,  we can now add the collision detection code to the rocket sprite.
![hit-rocks](https://salsa.debian.org/zleap-guest/scratch3_collision/-/raw/master/collision.png)

Essentially what we are doing here when the rocket touches the rocks, it says 'BOOM' for two seconds.  I have left this tutorial like this so you can make improvement as the whole purpose of scratch is to learn and a good way to learn is to experiment.

Try adding a second costume for the rocket sprite and change this when they touch,  perhaps also add some sound.

Have fun 
Created by Paul Sutton - e: [zleap@disroot.org](mailto:zleap@zdisroot.org) web: [https://personaljournal.ca/paulsutton/](https://personaljournal.ca/paulsutton/)

Please feel free to use, modify and contribute to this further.

![cc-logo](https://salsa.debian.org/zleap-guest/scratch3_collision/-/raw/master/88x31.png)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMzU0NzI3ODU2XX0=
-->
